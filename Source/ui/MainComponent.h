/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "FilePlayerGui.h"


#include "PEQSliderOne.h"
#include "PEQSliderTwo.h"
#include "PEQSliderThree.h"
#include "PEQSliderFour.h"



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public ComboBox::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);
    
    /** Destructor */
    ~MainComponent();
    
    void resized() override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    StringArray getMenuBarNames() override;
    
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    int getSelectedID();

    float setLowpassFilterFeedbackGain();
    
    void comboBoxChanged(ComboBox* comboBox);

private:
    Audio& audio;
    FilePlayerGui filePlayerGui;
  
    PEQSliderOne PEQSliderOne;
    PEQSliderTwo PEQSliderTwo;
    PEQSliderThree PEQSliderThree;
    PEQSliderFour PEQSliderFour;
  
    Label inputLabel;
    
    ComboBox inputBox;
    
    float lowpassGain;
    float highpassGain;
    float volume;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
