/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================

/** 
 Main Component Constructor,
 Implemeneted references to the classes I wish to control by initilising Main Component with the class references in its constructors arguments.
 */
MainComponent::MainComponent (Audio& audio_)
 :  audio (audio_),
    filePlayerGui (audio.getFilePlayer()),
    PEQSliderOne(audio.getParametricOne(), audio.getLowpassFilterOne(), audio.getHighpassFilterOne(), audio.getHighShelfFilterOne(), audio),
    PEQSliderTwo(audio.getParametricTwo(), audio.getLowpassFilterTwo(), audio.getHighpassFilterTwo(),  audio.getHighShelfFilterTwo(), audio),
    PEQSliderThree(audio.getParametricThree(), audio.getLowpassFilterThree(), audio.getHighpassFilterThree(),  audio.getHighShelfFilterThree(), audio),
    PEQSliderFour(audio.getParametricFour(), audio.getLowpassFilterFour(), audio.getHighpassFilterFour(),  audio.getHighShelfFilterFour(), audio)
{
    setSize (400, 400);
    addAndMakeVisible(filePlayerGui);
    
    addAndMakeVisible(&PEQSliderOne);
    addAndMakeVisible(&PEQSliderTwo);
    addAndMakeVisible(&PEQSliderThree);
    addAndMakeVisible(&PEQSliderFour);
    
    addAndMakeVisible(inputBox);
    inputBox.addItem("Microphone", 1);
    inputBox.addItem("File Player", 2);
    inputBox.addListener(this);
    inputBox.setSelectedId(1);
    
    inputLabel.setText("Select Input - >", dontSendNotification);
    inputLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&inputLabel);
}

/** */
MainComponent::~MainComponent()
{
    
}

/** */
void MainComponent::resized()
{
    Rectangle<int> r (getLocalBounds());
    
    filePlayerGui.setBounds (r.removeFromTop(20));
   
    PEQSliderOne.setBounds(0,50,1000,1000);
    PEQSliderTwo.setBounds(100,50,1000,1000);
    PEQSliderThree.setBounds(200, 50, 1000, 1000);
    PEQSliderFour.setBounds(300, 50, 1000, 1000);
    
    inputBox.setBounds(275,20,100,25);
    inputLabel.setBounds(170, 20, 100, 25);
}

void MainComponent::comboBoxChanged(ComboBox* comboBox)
{
    if (inputBox.getSelectedId() == 1)
    {
        audio.setInputBox(1);
    }
    else if (inputBox.getSelectedId() == 2)
    {
        audio.setInputBox(2);
        
    }
}

//MenuBarCallbacks==============================================================
/** */
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}
/** */
PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}
/** */
void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

