/*
  ==============================================================================

    Audio.cpp
    Created: 27/01/2016 
    Author:  Ricky Wayman

  ==============================================================================
*/

#define _USE_MATH_DEFINES

#include "Audio.h"
#include <math.h>
#include <complex>
/**
 Audio Class Constructor, Initilise device manager and source player here, assign audio callbacks to this class.
 */
Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&filePlayer);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}
/**
 Destructor
 */
Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}
/**
 Handle incoming midi messages, unused at present, will develop in the future.
 */
void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}
/**
 Filter Band One ComboBox / Filter Type Selector
 */
void Audio::getComboBox1(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox1 = val;
    
}
/**
 Filter Band Two ComboBox / Filter Type Selector
 */
void Audio::getComboBox2(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox2 = val;
    
}
/**
 Filter Band Three ComboBox / Filter Type Selector
 */
void Audio::getComboBox3(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox3 = val;
    
}
/**
 Filter Band Four ComboBox / Filter Type Selector
 */
void Audio::getComboBox4(int val)
{
    //DBG("AudioComboBoxValue: " << val);
    comboBox4 = val;
    
}
/**
 Input Selector ComboBox
 */
void Audio::setInputBox(int val)
{
    inputBox = val;
    DBG("inputbox val = " << val);
}
/**
 Audio Device Callback
 */
void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    /**
     This while loop is my audio processing loop which occurs at sample rate, 44100 times a second. All my audio processing takes place within this loop.
     */
    while(numSamples--)
    {
        /**
         Intput Selection
         */
        float input;
        if (inputBox == 1) // Microphone
        {
             input = *inL ;
        }
        else if (inputBox == 2) // File Player
        {
            input = *outL;
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------------
        /**
         Filter Band One
         */
        if (comboBox1 == 1) // LPF
        {
            stageOne = LPF1.process(input);
        }
        else if(comboBox1 == 2) // LSHLF
        {
            //stageOne = LSHLF1.process(input);
        }
        else if(comboBox1 == 3) // PEQ
        {
            stageOne = PEQ1.process(input);
        }
        else if(comboBox1 == 4) // HPF
        {
            stageOne = HPF1.process(input);
        }
        else if(comboBox1 == 5) // HSHLF
        {
            stageOne = HSHLF1.process(input);
        }
        else if(comboBox1 == 6) // DE ACTIVATE
        {
            
        }
         //--------------------------------------------------------------------------------------------------------------------------------------------------
        /**
         Filter Band Two
         */
        if (comboBox2 == 1) // LPF
        {
            stageTwo = LPF2.process(stageOne);
        }
        else if(comboBox2 == 2) // LSHLF
        {
            //stageTwo = LSHLF2.process(stageOne);
        }
        else if(comboBox2 == 3) // PEQ
        {
            stageTwo = PEQ2.process(stageOne);
        }
        else if(comboBox2 == 4) // HPF
        {
            stageTwo = HPF2.process(stageOne);
        }
        else if(comboBox2 == 5) // HSHLF
        {
            stageTwo = HSHLF2.process(stageOne);
        }
        else if(comboBox2 == 6) // DE ACTIVATE
        {
            
        }
         //--------------------------------------------------------------------------------------------------------------------------------------------------
        /**
         Filter Band Three
         */
        if (comboBox3 == 1) // LPF
        {
            stageThree = LPF3.process(stageTwo);
        }
        else if(comboBox3 == 2) // LSHLF
        {
            //stageThree = LSHLF3.process(stageTwo);
        }
        else if(comboBox3 == 3) // PEQ
        {
            stageThree = PEQ3.process(stageTwo);
        }
        else if(comboBox3 == 4) // HPF
        {
            stageThree = HPF3.process(stageTwo);
        }
        else if(comboBox3 == 5) // HSHLF
        {
            stageThree = HSHLF3.process(stageTwo);
        }
        else if(comboBox3 == 6) // DE ACTIVATE
        {
            
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------------
        /**
          Filter Band Four
         */
        if (comboBox4 == 1) // LPF
        {
            output = LPF4.process(stageThree);
        }
        else if(comboBox4 == 2) // LSHLF
        {
            //output = LSHLF4.process(stageThree);
        }
        else if(comboBox4 == 3) // PEQ
        {
            output = PEQ4.process(stageThree);
        }
        else if(comboBox4 == 4) // HPF
        {
            output = HPF4.process(stageThree);
        }
        else if(comboBox4 == 5) // HSHLF
        {
            output = HSHLF4.process(stageThree);
        }
        else if(comboBox1 == 6) // DE ACTIVATE
        {
            
        }
        /**
         Pointer to output channel, pass your final signal here to be output by the system.
         */
        *outL = output;//inSampL * 1.f;
        *outR = output;//inSampL * 1.f;
        
        /**
         Step initiation, bumps along one sample of buffer data to process next sample.
         */
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}