/*
  ==============================================================================

    Audio.h
    Created: 27/01/2016
    Author:  Ricky Wayman

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

#include "ParametricOne.h"
#include "ParametricTwo.h"
#include "ParametricThree.h"
#include "ParametricFour.h"

#include "LowpassFilterOne.h"
#include "LowpassFilterTwo.h"
#include "LowpassFilterThree.h"
#include "LowpassFilterFour.h"

#include "HighpassFilterOne.h"
#include "HighpassFilterTwo.h"
#include "HighpassFilterThree.h"
#include "HighpassFilterFour.h"

#include "LowShelfFilterOne.h"
#include "LowShelfFilterTwo.h"
#include "LowShelfFilterThree.h"
#include "LowShelfFilterFour.h"

#include "HighShelfFilterOne.h"
#include "HighShelfFilterTwo.h"
#include "HighShelfFilterThree.h"
#include "HighShelfFilterFour.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    /**
     Returns the file player which enables referencing for inter class communication.
     */
    FilePlayer& getFilePlayer() { return filePlayer; }
    /**
     Returns Parametric Filters which enables referencing for inter class communication.
     */
    ParametricOne& getParametricOne() { return PEQ1; }
    ParametricTwo& getParametricTwo() { return PEQ2; }
    ParametricThree& getParametricThree() { return PEQ3; }
    ParametricFour& getParametricFour() { return PEQ4; }
    /**
     Returns Lowpass Filters which enables referencing for inter class communication.
     */
    LowpassFilterOne& getLowpassFilterOne() { return LPF1; }
    LowpassFilterTwo& getLowpassFilterTwo() { return LPF2; }
    LowpassFilterThree& getLowpassFilterThree() { return LPF3; }
    LowpassFilterFour& getLowpassFilterFour() { return LPF4; }
    /**
     Returns Highpass Filters which enables referencing for inter class communication.
     */
    HighpassFilterOne& getHighpassFilterOne() { return HPF1; }
    HighpassFilterTwo& getHighpassFilterTwo() { return HPF2; }
    HighpassFilterThree& getHighpassFilterThree() { return HPF3; }
    HighpassFilterFour& getHighpassFilterFour() { return HPF4; }
    /**
     Returns Lowshelf Filters (UNUSED) which enables referencing for inter class communication.
     */
//    LowShelfFilterOne& getLowShelfFilterOne() { return LSHLF1; }
//    LowShelfFilterTwo& getLowShelfFilterTwo() { return LSHLF2; }
//    LowShelfFilterThree& getLowShelfFilterThree() { return LSHLF3; }
//    LowShelfFilterFour& getLowShelfFilterFour() { return LSHLF4; }
    /**
     Returns Highshelf Filters which enables referencing for inter class communication.
     */
    HighShelfFilterOne& getHighShelfFilterOne() { return HSHLF1; }
    HighShelfFilterTwo& getHighShelfFilterTwo() { return HSHLF2; }
    HighShelfFilterThree& getHighShelfFilterThree() { return HSHLF3; }
    HighShelfFilterFour& getHighShelfFilterFour() { return HSHLF4; }
    
    
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    void getComboBox1(int val);
    
    void getComboBox2(int val);
    
    void getComboBox3(int val);
    
    void getComboBox4(int val);
    
    void setInputBox(int val);
    
    
private:
    /**
     Creates an instance of Audio Device Manager.
     */
    AudioDeviceManager audioDeviceManager;
    /**
     Creates an instance of Audio Source Player.
     */
    AudioSourcePlayer audioSourcePlayer;
    /**
     Creates an instance of File Player.
     */
    FilePlayer filePlayer;
    /**
     Creates an instance of Parametric Filter.
     */
    ParametricOne PEQ1;
    ParametricTwo PEQ2;
    ParametricThree PEQ3;
    ParametricFour PEQ4;
    /**
     Creates an instance of Lowpass Filter.
     */
    LowpassFilterOne LPF1;
    LowpassFilterTwo LPF2;
    LowpassFilterThree LPF3;
    LowpassFilterFour LPF4;
    /**
     Creates an instance of Lowshelf Filter.
     */
//    LowShelfFilterOne LSHLF1:
//    LowShelfFilterTwo LSHLF2:
//    LowShelfFilterThree LSHLF3:
//    LowShelfFilterFour LSHLF4:
    
    /**
     Creates an instance of Highpass Filter.
     */
    HighpassFilterOne HPF1;
    HighpassFilterTwo HPF2;
    HighpassFilterThree HPF3;
    HighpassFilterFour HPF4;
    /**
     Creates an instance of Highshelf Filter.
     */
    HighShelfFilterOne HSHLF1;
    HighShelfFilterTwo HSHLF2;
    HighShelfFilterThree HSHLF3;
    HighShelfFilterFour HSHLF4;
    /**
     ComboBox Member Variables
     */
    int comboBox1;
    int comboBox2;
    int comboBox3;
    int comboBox4;
    int inputBox;
    /**
     Output Stage Member Variables
     */
    double output;
    double stageOne;
    double stageTwo;
    double stageThree;
    
};



#endif  // AUDIO_H_INCLUDED
