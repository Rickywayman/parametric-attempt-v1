//
//  PEQSliderOne.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#include "PEQSliderOne.h"
#include <math.h>

/** Constructor */
PEQSliderOne::PEQSliderOne (ParametricOne& PEQ_,
                            LowpassFilterOne& LPF_,
                            HighpassFilterOne& HPF_,
                            
                            HighShelfFilterOne& HSHLF_,
                            Audio& audio_)
    :
    audioRef(audio_),
    PEQ(PEQ_),
    LPF(LPF_),
    HPF(HPF_),
    
    HSHLF(HSHLF_)
{
    
    // Gain Slider
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    gainSlider.setValue(0.50);
    gainSlider.addListener (this);
    gainSlider.setDoubleClickReturnValue(true, 0.50);
  
    // Q Slider
    addAndMakeVisible(&QSlider);
    QSlider.setRange(1.0, 10.0, 1.0);
    QSlider.setSliderStyle(Slider::LinearBar);
    QSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    QSlider.setValue(2.0);
    QSlider.addListener(this);
    QSlider.setDoubleClickReturnValue(true, 2.0);
    QSlider.setTextBoxIsEditable(false);
    
    // Feedback Slider
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(20.0, 20000.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackGainSlider.setValue(1000.0);
    feedbackGainSlider.setDoubleClickReturnValue(true, 1000.0);
    
    // Gain Label
    gainLabel.setText("Gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    
    // Feedback Label
    feedbackGainLabel.setText("Freq", dontSendNotification);
    feedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackGainLabel);
    
    // Bandpass Q Label
    bandpassQLabel.setText("Q", dontSendNotification);
    bandpassQLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&bandpassQLabel);
    
    // Band Label
    bandLabel.setText("1", dontSendNotification);
    bandLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(&bandLabel);
    
    // Filter Type Select Combo Box
    addAndMakeVisible(filterType);
    filterType.addItem("LPF", 1);
    filterType.addItem("LSHLF", 2);
    filterType.addItem("PEQ", 3);
    filterType.addItem("HPF", 4);
    filterType.addItem("HSHLF", 5);
    filterType.addItem("OFF", 6);
    filterType.setSelectedId(3);
    filterType.addListener(this);
    
}
/** Destructor */
PEQSliderOne::~PEQSliderOne()
{
    
}

/** */
void PEQSliderOne::resized()
{
    Rectangle<int> r (getLocalBounds());
    
    feedbackGainLabel.setBounds(10,0,70,15);
    feedbackGainSlider.setBounds(10,20,70,70);
    gainLabel.setBounds(10,100,70,15);
    gainSlider.setBounds(10,120,70,70);
    bandpassQLabel.setBounds(10,200,70,15);
    QSlider.setBounds(10,220,70,30);
    filterType.setBounds(10, 260, 70, 30);
    bandLabel.setBounds(10, 280, 70, 70);
}

/** 
 Values created with sliders are registered with the slider Listener in Slider Value Changed.
 These are then parsed to their appropriate fliter classes for filter configuration and processing where they will become the input values to the Bi-quad coefficients.
 
 */
void PEQSliderOne::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider)
    {
        gain = slider->getValue();
        
        Gain = 20 * gain - 10;
        
        PEQ.config(feedbackGain, 44100, bandpassQ, Gain);
        
        LPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HSHLF.config(feedbackGain, 44100, bandpassQ, Gain);
    }
    else if (slider == &feedbackGainSlider)
    {
        feedbackGain = slider->getValue();
        
        PEQ.config(feedbackGain, 44100, bandpassQ, Gain);
        
        LPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HSHLF.config(feedbackGain, 44100, bandpassQ, Gain);
    }
    else if (slider == &QSlider)
    {
        bandpassQ = slider->getValue();
        
        PEQ.config(feedbackGain, 44100, bandpassQ, Gain);
        
        LPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HPF.config(feedbackGain, 44100, bandpassQ, Gain);
        
        HSHLF.config(feedbackGain, 44100, bandpassQ, Gain);
    }
}
/**
 Combo Box callback function parses combobox int val data to audio by reference to audio, audioRef. This then changes the type of filter processing on the output in the audio callback function.
 */
void PEQSliderOne::comboBoxChanged(ComboBox* filterType)
{
    if (filterType->getSelectedId() == 1) // LPF
    {
        audioRef.getComboBox1(1);
    }
    else if(filterType->getSelectedId() == 2) // LSHLF
    {
        audioRef.getComboBox1(2);
    }
    else if(filterType->getSelectedId() == 3) // PEQ
    {
        audioRef.getComboBox1(3);
    }
    else if(filterType->getSelectedId() == 4) // HPF
    {
        audioRef.getComboBox1(4);
    }
    else if(filterType->getSelectedId() == 5) // HSHLF
    {
        audioRef.getComboBox1(5);
    }
    else if(filterType->getSelectedId() == 6) // OFF
    {
        audioRef.getComboBox1(6);
    }

}

// Class End
