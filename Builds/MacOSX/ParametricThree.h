//
//  ParametricThree.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__ParametricThree__
#define __JuceBasicAudio__ParametricThree__


#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Parametric Filter Class Three
 */
class ParametricThree

{
public:
    //==============================================================================
    /**
     Constructor
     */
    ParametricThree();
    
    /**
     Destructor
     */
    ~ParametricThree();
    //==============================================================================
    /**
     Member Functions   
     */
    void config(double fc, double fs, double q, double gain);
    double process(double x0);
    
    void centerFrequency(double val);
    void quality(double val);
    /**
     Member Variables
     */
private:
    double a0, a1, a2, b1 ,b2, c0, d0;
    double x1, x2, y1, y2;
    double cf;
    double q;
    
};

#endif /* defined(__JuceBasicAudio__ParametricThree__) */
