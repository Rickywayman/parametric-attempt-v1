//
//  HighpassFilterTwo.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 28/01/2016.
//
//

#include "HighpassFilterTwo.h"


/** Highpass Filter Class Two */

/** Constructor initilizes all filter coefficients variables to zero / Initilizes all shift register variables to zero. Initilizes filter config to 1kHz, 44.1kHz SR, 0.0 Gain */
HighpassFilterTwo::HighpassFilterTwo()
{
    a0 = a1 = a2 = b1 = b2 = 0;
    x1 = x2 = y1 = y2 = 0;
    
    config(1000, 44100, 10, 0.0);
}

/** */
HighpassFilterTwo::~HighpassFilterTwo()
{
    
}

/** Process funtion that passes the input through the filters transfer function and shifts the register along one, giving us our sample delay. */
void HighpassFilterTwo::config(double fc, double fs, double Q, double gain)
{
    double theta;
    double gamma;
    
    DBG("Highpass fc = " << fc);
    
    theta = ( 2.0 * M_PI * fc ) / fs;
    gamma = ( cos ( theta ) ) / ( 1 + sin ( theta ) );
    a0 = ( 1 + gamma ) / 2;
    a1 = -1 * ( ( 1 + gamma ) / 2 );
    a2 = 0.0;
    b1 = -1 * gamma;
    b2 = 0.0;

    
}

/** Process funtion that passes the input through the filters transfer function and shifts the register along one, giving us our sample delay. */
double HighpassFilterTwo::process(double x0)
{
    double y0 = a0 * x0 + a1 * x1 + a2 * x2 - b1 * y1 - b2 * y2;
    
    x2 = x1;
    x1 = x0;
    y2 = y1;
    y1 = y0;
    
    return y0;
}


