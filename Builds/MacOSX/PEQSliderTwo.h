//
//  PEQSliderTwo.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__PEQSliderTwo__
#define __JuceBasicAudio__PEQSliderTwo__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "ParametricTwo.h"
#include "LowpassFilterTwo.h"
#include "HighpassFilterTwo.h"
#include "LowShelfFilterTwo.h"
#include "HighShelfFilterTwo.h"

class PEQSliderTwo :        public Component,
                            private Slider::Listener,
                            public ComboBox::Listener
{
public:
    /**
     Inside the constructor I have referenced the class instances to be able to assign data to them directly.
     */
    PEQSliderTwo  (ParametricTwo& PEQ_,
                   LowpassFilterTwo& LPF_,
                   HighpassFilterTwo& HPF_,
                   
                   HighShelfFilterTwo& HSHLF_,
                   Audio& audio_);
    
    ~PEQSliderTwo();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
    void comboBoxChanged(ComboBox* filterType);
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PEQSliderTwo)
    
    void sliderValueChanged	(Slider * 	slider);
    
    Audio& audioRef;
    ParametricTwo& PEQ;
    LowpassFilterTwo& LPF;
    HighpassFilterTwo& HPF;
    //LowShelfFilterTwo& LSHLF;
    HighShelfFilterTwo& HSHLF;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    Label bandLabel;
    
    ComboBox filterType;
    
    float gain;
    float level;
    double cornerFrequency;
    double bandpassQ;
    double Gain;
    double feedbackGain;
    
};

#endif /* defined(__JuceBasicAudio__PEQSliderTwo__) */
