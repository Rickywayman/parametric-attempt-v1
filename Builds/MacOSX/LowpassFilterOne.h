//
//  LowpassFilterOne.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__LowpassFilterOne__
#define __JuceBasicAudio__LowpassFilterOne__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Lowpass Filter Class One
 */
class LowpassFilterOne

{
public:
    //==============================================================================
    /**
     Constructor
     */
    LowpassFilterOne();
    
    /**
     Destructor
     */
    ~LowpassFilterOne();
    //==============================================================================
    /**
     Member Functions
     */
    void config(double fc, double fs, double q, double gain);
    double process(double x0);
    
    void centerFrequency(double val);
    void quality(double val);
    
private:
    double a0, a1, a2, b1 ,b2;
    double x1, x2, y1, y2;
    double cf;
    double q;
    
};

#endif /* defined(__JuceBasicAudio__LowpassFilterOne__) */
