//
//  Parametric.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#include "ParametricOne.h"

/** Parametric Filter Class One */

/** Constructor initilizes all filter coefficients variables to zero / Initilizes all shift register variables to zero. Initilizes filter config to 1kHz, 44.1kHz SR, 0.0 Gain */
ParametricOne::ParametricOne()
{
    a0 = a1 = a2 = b1 = b2 = c0 = d0 = 0;
    x1 = x2 = y1 = y2 = 0;
    
    config(1000, 44100, 10, 0.0);
}

/** Destructor */
ParametricOne::~ParametricOne()
{
    
}

/** Parametric One Configuration Function */

/** This function inserts the variables to the equations that calcuate the Biquadratic Coefficients derived from the user input of Frequency, Sample Rate & Gain */
void ParametricOne::config(double fc, double fs, double Q, double gain)
{
    double theta;
    double beta;
    double gamma;
    double mu;
    double zeta;
    
    DBG("ONE fc = " << fc);
    DBG("ONE q = " << Q);
    DBG("ONE gain = " << gain);
    
    theta = ( 2.0 * M_PI * fc ) / fs;
    mu = pow ( 10, ( gain / 20 ) );
    zeta = 4 / ( 1 + mu );
    beta = ( ( 1 - ( zeta * ( tan ( theta / ( 2 * Q ) ) ) ) ) / ( 1 + ( zeta * ( tan ( theta / ( 2 * Q ) ) ) ) ) ) * 0.5;
    gamma = ( 0.5 + beta ) * cos ( theta );
    a0 = 0.5 - beta;
    a1 = 0.0;
    a2 = -1 * ( 0.5 - beta );
    b1 = -2 * gamma;
    b2 = 2 * beta;
    c0 = mu - 1.0;
    d0 = 1.0;
}

/** Process funtion that passes the input through the filters transfer function and shifts the register along one, giving us our sample delay. */
double ParametricOne::process(double x0)
{
    double y0 = a0 * x0 + a1 * x1 + a2 * x2 - b1 * y1 - b2 * y2;
    
    x2 = x1;
    x1 = x0;
    y2 = y1;
    y1 = y0;
    
    return (y0 * c0) + (d0 * x0);
}


