//
//  PEQSliderOne.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 25/01/2016.
//
//

#ifndef __JuceBasicAudio__PEQSliderOne__
#define __JuceBasicAudio__PEQSliderOne__

#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "ParametricOne.h"
#include "LowpassFilterOne.h"
#include "HighpassFilterOne.h"
#include "LowShelfFilterOne.h"
#include "HighShelfFilterOne.h"

class PEQSliderOne :        public Component,
                            private Slider::Listener,
                            public ComboBox::Listener
{
public:
    /** 
     Inside the constructor I have referenced the class instances to be able to assign data to them directly.
     */
    PEQSliderOne  (ParametricOne& PEQ_,
                   LowpassFilterOne& LPF_,
                   HighpassFilterOne& HPF_,
                   
                   HighShelfFilterOne& HSHLF_,
                   Audio& audio_);
    
    ~PEQSliderOne();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
    void comboBoxChanged(ComboBox* filterType);
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PEQSliderOne)
    
    void sliderValueChanged	(Slider * 	slider);
    
    Audio& audioRef;
    ParametricOne& PEQ;
    LowpassFilterOne& LPF;
    HighpassFilterOne& HPF;
    //LowShelfFilterOne& LSHLF;
    HighShelfFilterOne& HSHLF;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    Label bandLabel;
    
    ComboBox filterType;
    
    float gain;
    float level;
    double cornerFrequency;
    double bandpassQ;
    double Gain;
    double feedbackGain;
    
};





#endif /* defined(__JuceBasicAudio__PEQSliderOne__) */
