//
//  PEQSliderThree.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__PEQSliderThree__
#define __JuceBasicAudio__PEQSliderThree__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "ParametricThree.h"
#include "LowpassFilterThree.h"
#include "HighpassFilterThree.h"
#include "LowShelfFilterThree.h"
#include "HighShelfFilterThree.h"

class PEQSliderThree :      public Component,
                            private Slider::Listener,
                            public ComboBox::Listener
{
public:
    /**
     Inside the constructor I have referenced the class instances to be able to assign data to them directly.
     */
    PEQSliderThree  (ParametricThree& PEQ_,
                     LowpassFilterThree& LPF_,
                     HighpassFilterThree& HPF_,
                     
                     HighShelfFilterThree& HSHLF_,
                     Audio& audio_);
    
    ~PEQSliderThree();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
    void comboBoxChanged(ComboBox* filterType);
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PEQSliderThree)
    
    void sliderValueChanged	(Slider * 	slider);
   
    Audio& audioRef;
    ParametricThree& PEQ;
    LowpassFilterThree& LPF;
    HighpassFilterThree& HPF;
    //LowShelfFilterThree& LSHLF;
    HighShelfFilterThree& HSHLF;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    Label bandLabel;
    
    ComboBox filterType;
    
    float gain;
    float level;
    double cornerFrequency;
    double bandpassQ;
    double Gain;
    double feedbackGain;
    
};

#endif /* defined(__JuceBasicAudio__PEQSliderThree__) */
