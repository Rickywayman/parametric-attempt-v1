//
//  LowpassFilter2.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 28/01/2016.
//
//

#ifndef __JuceBasicAudio__LowpassFilter2__
#define __JuceBasicAudio__LowpassFilter2__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Lowpass Filter Class Two
 */
class LowpassFilterTwo

{
public:
    //==============================================================================
    /**
     Constructor
     */
    LowpassFilterTwo();
    
    /**
     Destructor
     */
    ~LowpassFilterTwo();
    //==============================================================================
    /**
     Member Functions
     */
    void config(double fc, double fs, double q, double gain);
    double process(double x0);
    
    void centerFrequency(double val);
    void quality(double val);
    
private:
    double a0, a1, a2, b1 ,b2;
    double x1, x2, y1, y2;
    double cf;
    double q;
    
};

#endif /* defined(__JuceBasicAudio__LowpassFilter2__) */
