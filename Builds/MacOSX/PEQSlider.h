//
//  PEQSlider.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__PEQSlider__
#define __JuceBasicAudio__PEQSlider__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "ParametricFour.h"

class PEQSlider :       public Component,
                        private Slider::Listener
{
public:
    
    PEQSlider  (ParametricFour& PEQ_, Audio& audio_
                    );
    ~PEQSlider();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PEQSlider)
    
    void sliderValueChanged	(Slider * 	slider);
    
    Audio& audioRef;
    
    ParametricFour& PEQ;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    
    float gain;
    float level;
    double cornerFrequency;
    double bandpassQ;
    double PEQGain;
    double feedbackGain;
    
};



#endif /* defined(__JuceBasicAudio__PEQSlider__) */
