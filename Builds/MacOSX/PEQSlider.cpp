//
//  PEQSlider.cpp
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#include "PEQSlider.h"
#include <math.h>

/** Constructor */
PEQSlider::PEQSlider (ParametricFour& PEQ_, Audio& audio_)
:
audioRef(audio_),
PEQ(PEQ_)
{
    
    // Gain Slider
    addAndMakeVisible(&gainSlider);
    gainSlider.setRange(0.0, 1.0, 0.01);
    gainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    gainSlider.setValue(0.50);
    //gainSlider.addListener(this);
    
    // Q Slider
    addAndMakeVisible(&QSlider);
    QSlider.setRange(1.0, 10.0, 0.01);
    QSlider.setSliderStyle(Slider::LinearBar);
    QSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    QSlider.setValue(2.0);
    QSlider.addListener(this);
    
    // Feedback Slider
    addAndMakeVisible(&feedbackGainSlider);
    feedbackGainSlider.setRange(20.0, 20000.0, 0.01);
    feedbackGainSlider.setSliderStyle(Slider::RotaryVerticalDrag);
    feedbackGainSlider.addListener(this);
    feedbackGainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 100, 20);
    feedbackGainSlider.setValue(1000.0);
    
    // Gain Label
    gainLabel.setText("Gain", dontSendNotification);
    gainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&gainLabel);
    gainSlider.addListener (this);
    
    // Feedback Label
    feedbackGainLabel.setText("Freq", dontSendNotification);
    feedbackGainLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&feedbackGainLabel);
    
    // Bandpass Q Label
    bandpassQLabel.setText("Q", dontSendNotification);
    bandpassQLabel.setJustificationType( Justification::centred);
    addAndMakeVisible(&bandpassQLabel);
    
}
/** Destructor */
PEQSlider::~PEQSlider()
{
    
}

/** */
void PEQSlider::resized()
{
    Rectangle<int> r (getLocalBounds());
    
    feedbackGainLabel.setBounds(10,0,70,15);
    feedbackGainSlider.setBounds(10,20,70,70);
    gainLabel.setBounds(10,100,70,15);
    gainSlider.setBounds(10,120,70,70);
    bandpassQLabel.setBounds(10,200,70,15);
    QSlider.setBounds(10,220,70,30);
}

/** */
void PEQSlider::sliderValueChanged (Slider* slider)
{
    if (slider == &gainSlider)
    {
        gain = slider->getValue();
        
        PEQGain = 20 * gain - 10;
        
        PEQ.config(feedbackGain, 44100, bandpassQ, PEQGain);
    }
    else if (slider == &feedbackGainSlider)
    {
        feedbackGain = slider->getValue();
        
        PEQ.config(feedbackGain, 44100, bandpassQ, PEQGain);
    }
    else if (slider == &QSlider)
    {
        bandpassQ = slider->getValue();
        
        PEQ.config(feedbackGain, 44100, bandpassQ, PEQGain);
    }
}

// Class End
