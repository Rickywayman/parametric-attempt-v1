//
//  PEQSliderFour.h
//  JuceBasicAudio
//
//  Created by Ricky Wayman on 27/01/2016.
//
//

#ifndef __JuceBasicAudio__PEQSliderFour__
#define __JuceBasicAudio__PEQSliderFour__

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Audio.h"
#include "ParametricFour.h"
#include "LowpassFilterFour.h"
#include "HighpassFilterFour.h"
#include "LowShelfFilterFour.h"
#include "HighShelfFilterFour.h"

class PEQSliderFour :       public Component,
                            private Slider::Listener,
                            public ComboBox::Listener
{
public:
    /**
     Inside the constructor I have referenced the class instances to be able to assign data to them directly.
     */
    PEQSliderFour  (ParametricFour& PEQ_,
                    LowpassFilterFour& LPF_,
                    HighpassFilterFour& HPF_,
                    
                    HighShelfFilterFour& HSHLF_,
                    Audio& audio_);
    
    ~PEQSliderFour();
    
    void resized() override;
    
    float getLowpassFeedbackGain();
    
    float getLowpassGain();
    
    void comboBoxChanged(ComboBox* filterType);
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PEQSliderFour)
    
    void sliderValueChanged	(Slider * 	slider);
    
    Audio& audioRef;
    ParametricFour& PEQ;
    LowpassFilterFour& LPF;
    HighpassFilterFour& HPF;
    //LowShelfFilterFour& LSHLF;
    HighShelfFilterFour& HSHLF;
    
    Slider gainSlider;
    Slider feedbackGainSlider;
    Slider QSlider;
    Label  gainLabel;
    Label feedbackGainLabel;
    Label bandpassQLabel;
    Label bandLabel;
    
    ComboBox filterType;
    
    float gain;
    float level;
    double cornerFrequency;
    double bandpassQ;
    double Gain;
    double feedbackGain;
    
};

#endif /* defined(__JuceBasicAudio__PEQSliderFour__) */
